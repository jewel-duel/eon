/**
 * # General
 *
 * ## Connecting to the Websocket Server
 *
 * 1. User Connects to websocket server.
 * 2. [UserInfoQuery](classes/userinfoquery.html) is automatically sent to the user.
 * 3. The User should respond with a [UserInfoResponse](classes/userinforesponse.html) Response.
 *
 * @mermaid
 * sequenceDiagram
 *  autonumber
 *  Client->>Server: Connection
 *  Server-->>+Client: UserInfoQuery
 *  Client-->>-Server: UserInfoResponse
 *
 * @module Workflows/General
 */

/**
 * # Room Events
 *
 * ## Creating a Room
 *
 * Any user can create a room.
 *
 * 1. User sends a [CreateRoomRequest](classes/createroomrequest.html).
 * 2. If successful, server responds with a [CreateRoomSuccess](classes/createroomsuccess.html) Response.
 * 3. If unsuccessful, server responds with a [CreateRoomError](classes/createroomerror.html) Response.
 *
 * <div class="mermaid">
 * sequenceDiagram
 *   autonumber
 *   Client->>Server: CreateRoomRequest
 *   rect rgb(117, 255, 51)
 *     alt Successful
 *       Server-->>Client: CreateRoomSuccess
 *     end
 *   end
 *   rect rgb(255, 51, 0)
 *     alt Unsuccessful
 *       Server-->>Client: CreateRoomError
 *     end
 *   end
 * </div>
 *
 * ## Joining a Room
 *
 * Any user can join a room.
 *
 * 1. User sends a [JoinRoomRequest](classes/joinroomrequest.html).
 * 2. If unsuccessful, server responds with a [JoinRoomError](classes/joinroomerror.html) Response.
 * 3. If successful, server responds with a [JoinRoomSuccess](classes/joinroomsuccess.html) Response.
 * 4. All users notified with a {@link JoinedRoomNotification} when a user joins.
 *
 * <div class="mermaid">
 * sequenceDiagram
 *  autonumber
 *  Client->>Server: JoinRoomRequest
 *  rect rgb(255, 51, 0)
 *    Server-->>Client: JoinRoomError
 *  end
 *  rect rgb(117, 255, 51)
 *    Server-->>Client: JoinRoomSuccess
 *    Server-->>All: JoinedRoomNotification
 *  end
 * </div>
 *
 * ## Leaving a Room
 *
 * A User in a room can leave at any time. The other users are notified with a [UserLeftRoomNotification](classes/userleftroomnotification.html) when this happens.
 *
 * If the user who left was the host of the room, a new host is assigned by the server and a [HostChangedNotification](classes/hostchangednotification.html) is sent to all of the users in the room.
 *
 * <div class="mermaid">
 * sequenceDiagram
 *  autonumber
 *  Note over Server,All: User Leaves a Room
 *  Server-->>All: UserLeftRoomNotification
 *  opt Host has left
 *   Server-->>All: HostChangedNotification
 *  end
 * </div>
 *
 * ## Sending a Chat
 *
 * A user can send a chat message to all the others users in a room.
 *
 * <div class="mermaid">
 * sequenceDiagram
 * autonumber
 *
 * </div>
 *
 * ## Keypresses
 *
 * ## Changing Game Options
 *
 * ## Changing Player Color
 *
 * ## Setting Ready State for a Player
 *
 * ## Starting the Game
 *
 * @module Workflows/Room
 */

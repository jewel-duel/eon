/**
 * This is the doc comment for game.ts
 * 
 * @mermaid Make TypeDoc easy to use with mermaid.js
 * sequenceDiagram
 *  Alice->>+John: Hello John, how are you?
 *  John-->>-Alice: Great!
 * 
 * @module Workflows/Game
 */

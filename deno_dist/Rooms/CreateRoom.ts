import {
  BaseCommand,
  BaseResponse,
  CommandType,
  ICommand,
  IError,
  IResponse,
} from "../Command";
import type { IUser } from "../Users/User";
import type { IPlayer } from "../Users/Player";

export interface ICreateRoomRequestData {
  user: IUser;
}

/** A request to create a room. */
export class CreateRoomRequest extends BaseCommand implements ICommand {
  constructor(public data: ICreateRoomRequestData) {
    super(CommandType.CreateRoomRequest);
  }
}

export interface ICreateRoomSuccessData {
  key: string;
  host: IPlayer;
}

/**
 * A success response if the room was created.
 *
 * @export
 * @class CreateRoomSuccess
 * @extends {BaseResponse}
 * @implements {IResponse}
 */
export class CreateRoomSuccess extends BaseResponse implements IResponse {
  constructor(public data: ICreateRoomSuccessData) {
    super(true, CommandType.CreateRoomSuccess);
  }
}

export interface ICreateRoomErrorData {
  error: IError;
}

/**
 * An error response if the room cannot be created.
 *
 * @export
 * @class CreateRoomError
 * @extends {BaseResponse}
 * @implements {IResponse}
 */
export class CreateRoomError extends BaseResponse implements IResponse {
  constructor(public data: IError) {
    super(false, CommandType.CreateRoomError);
  }
}

import {
  BaseCommand,
  ICommand,
  CommandType,
  BaseResponse,
  IResponse,
  IError,
} from "../Command.ts";
import type { IUser } from "../Users/User.ts";
import type { IPlayer } from "../Users/Player.ts";

//#region Request

export interface IJoinRoomRequestData {
  key: string;
  user: IUser;
}

export class JoinRoomRequest extends BaseCommand implements ICommand {
  public constructor(public data: IJoinRoomRequestData) {
    super(CommandType.JoinRoomRequest);
  }
}

//#endregion

//#region Response

export interface IJoinRoomSuccessData {
  key: string;
  host: IPlayer;
  guests: IPlayer[];
  game?: any;
}

export class JoinRoomSuccess extends BaseResponse implements IResponse {
  constructor(public data: IJoinRoomSuccessData) {
    super(true, CommandType.JoinRoomSuccess);
  }
}

export class JoinRoomError extends BaseResponse implements IResponse {
  constructor(public data: IError) {
    super(false, CommandType.JoinRoomError);
  }
}

//#endregion Response

//#region Notification

export interface IJoinedRoomNotification {
  user: IPlayer;
}

export class JoinedRoomNotification extends BaseCommand implements ICommand {
  constructor(public data: IJoinedRoomNotification) {
    super(CommandType.JoinedRoomNotification);
  }
}

//#endregion

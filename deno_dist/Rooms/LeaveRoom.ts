import { CommandType, BaseCommand, ICommand, IBroadcastData } from "../Command.ts";

export interface IUserLeaveRoomBroadcastData extends IBroadcastData {
  roomKey: string;
  userId: string;
}

export class UserLeavingRoomBroadcast extends BaseCommand implements ICommand {
  constructor(public data: IUserLeaveRoomBroadcastData) {
    super(CommandType.UserLeavingRoomBroadcast);
  }
}

export interface IUserLeftRoomNotificationData {
  userId: string;
}

export class UserLeftRoomNotification extends BaseCommand implements ICommand {
  constructor(public data: IUserLeftRoomNotificationData) {
    super(CommandType.UserLeftRoomNotification);
  }
}

export interface IHostChangedNotificationData {
  userId: string;
}

export class HostChangedNotification extends BaseCommand implements ICommand {
  constructor(public data: IHostChangedNotificationData) {
    super(CommandType.HostChangedNotification);
  }
}

export interface ICommand {
  type: CommandType;
  createdAt: number;
  data: { [key: string]: any };

  toString(): string;
}

export interface IBroadcastData {
  room: string;
}

export interface IResponse extends ICommand {
  success: boolean;
}

export interface IError {
  info: { [key: string]: any };
}

export enum CommandType {
  ChatSentBroadcast = "ChatSentBroadcast",
  CreateRoomRequest = "CreateRoomRequest",
  CreateRoomSuccess = "CreateRoomSuccess",
  CreateRoomError = "CreateRoomError",
  FlipTileRequest = "FlipTileRequest",
  TileFlippedNotification = "TileFlippedNotification",
  HostChangedNotification = "HostChangedNotification",
  JoinRoomRequest = "JoinRoomRequest",
  JoinRoomSuccess = "JoinRoomSuccess",
  JoinRoomError = "JoinRoomError",
  JoinedRoomNotification = "JoinedRoomNotification",
  SendChatRequest = "SendChatRequest",
  SendChatSuccess = "SendChatSuccess",
  SendChatError = "SendChatError",
  UserReadinessBroadcast = "UserReadinessBroadcast",
  UserReadyNotification = "UserReadyNotification",
  UserLeavingRoomBroadcast = "UserLeavingRoomBroadcast",
  UserLeftRoomNotification = "UserLeftRoomNotification",
  UserPressingKeyBroadcast = "UserPressingKeyBroadcast",
  UserPressedKeyNotification = "UserPressedKeyNotification",
  UserInfoQuery = "UserInfoQuery",
  UserInfoResponse = "UserInfoResponse",
  StartGameRequest = "StartGameRequest",
  StartGameSuccess = "StartGameSuccess",
  StartGameError = "StartGameError",
  GameStartedNotification = "GameStartedNotification",
}

export abstract class BaseCommand implements ICommand {
  public createdAt: number;
  abstract data: { [key: string]: any };

  protected constructor(public type: CommandType) {
    this.createdAt = Date.now();
  }

  public toString() {
    return JSON.stringify(this);
  }
}

export abstract class BaseResponse extends BaseCommand implements IResponse {
  protected constructor(public success: boolean, type: CommandType) {
    super(type);
  }
}

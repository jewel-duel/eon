import { BaseCommand, ICommand, CommandType } from "../Command.ts";

export interface IUser {
  id: string;
  name: string;
}

export class User implements IUser {
  constructor(public id: string, public name: string) {}
}

export interface IUserInfoQueryData {}

export class UserInfoQuery extends BaseCommand implements ICommand {
  constructor(public data: IUserInfoQueryData) {
    super(CommandType.UserInfoQuery);
  }
}

export interface IUserInfoResponseData {
  user: IUser;
}

export class UserInfoResponse extends BaseCommand implements ICommand {
  constructor(public data: IUserInfoResponseData) {
    super(CommandType.UserInfoResponse);
  }
}

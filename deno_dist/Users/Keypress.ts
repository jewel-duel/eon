import { IBroadcastData, BaseCommand, CommandType, ICommand } from "../Command.ts";

export interface IUserPressingKeyBroadcastData extends IBroadcastData {
  userId: string;
  roomKey: string;
}

export class UserPressingKeyBroadcast extends BaseCommand implements ICommand {
  constructor(public data: IUserPressingKeyBroadcastData) {
    super(CommandType.UserPressingKeyBroadcast);
  }
}

export interface IUserPressedKeyNotificationData {
  userId: string;
}

export class UserPressedNotification extends BaseCommand implements ICommand {
  constructor(public data: IUserPressedKeyNotificationData) {
    super(CommandType.UserPressedKeyNotification);
  }
}

import type { IUser } from "./User.ts";
import { BaseCommand, CommandType, ICommand, IBroadcastData } from "../Command.ts";

export interface IUserReadyBroadcastData extends IBroadcastData {
  roomKey: string;
  user: IUser;
}

export class UserReadyBroadcast extends BaseCommand implements ICommand {
  constructor(public data: IUserReadyBroadcastData) {
    super(CommandType.UserReadinessBroadcast);
  }
}

export interface IUserReadyNotificationData {
  user: IUser;
  readiness: boolean;
}

export class UserReadyNotification extends BaseCommand implements ICommand {
  constructor(public data: IUserReadyNotificationData) {
    super(CommandType.UserReadyNotification);
  }
}

export interface IPlayer {
  id: string;
  name: string;
  color: string;
}

export class Player implements IPlayer {
  constructor(public id: string, public name: string, public color: string) {}
}

import { BaseCommand, ICommand, CommandType } from "../Command.ts";

export interface IFlipTileRequestData {
  gameKey: string;
  tileCoordinates: string;
}

export class FlipTileRequest extends BaseCommand implements ICommand {
  constructor(public data: IFlipTileRequestData) {
    super(CommandType.FlipTileRequest);
  }
}

export interface ITileFlippedNotification {
  tileCoordinates: string;
}

export class TileFlippedNotification extends BaseCommand implements ICommand {
  constructor(public data: ITileFlippedNotification) {
    super(CommandType.TileFlippedNotification);
  }
}

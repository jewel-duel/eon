import type { IGameOptions } from "./Options.ts";
import {
  BaseCommand,
  ICommand,
  CommandType,
  BaseResponse,
  IResponse,
  IError,
} from "../Command.ts";
import type { IPlayer } from "../Users/Player.ts";

export interface IStartGameRequestData {
  players: IPlayer[];
  roomKey: string;
  options: IGameOptions;
}

export class StartGameRequest extends BaseCommand implements ICommand {
  constructor(public data: IStartGameRequestData) {
    super(CommandType.StartGameRequest);
  }
}

export interface IStartGameSuccessData {}

export class StartGameSuccess extends BaseResponse implements IResponse {
  constructor(public data: IStartGameSuccessData) {
    super(true, CommandType.StartGameSuccess);
  }
}

export interface IStartGameErrorData {
  error: IError;
}

export class StartGameError extends BaseResponse implements IResponse {
  constructor(public data: IStartGameErrorData) {
    super(false, CommandType.StartGameError);
  }
}

export interface IGameStartedNotificationData {
  key: string;
  game: any;
}

export class GameStartedNotification extends BaseCommand implements ICommand {
  constructor(public data: IGameStartedNotificationData) {
    super(CommandType.GameStartedNotification);
  }
}

export interface IGameGenerationOptions {
  key: string;
  options: { [key: string]: any };
}

export interface IGameOptions {
  generation: IGameGenerationOptions;
}

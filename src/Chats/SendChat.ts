import {
  BaseCommand,
  BaseResponse,
  CommandType,
  ICommand,
  IError,
  IResponse,
} from "../Command";

export interface ISendChatRequestData {
  userId: string;
  roomKey: string;
  chat: string;
}

export class SendChatRequest extends BaseCommand implements ICommand {
  constructor(public data: ISendChatRequestData) {
    super(CommandType.SendChatRequest);
  }
}

export interface ISendChatSuccessData {}

export class SendChatSuccess extends BaseResponse implements IResponse {
  constructor(public data: ISendChatRequestData) {
    super(true, CommandType.SendChatSuccess);
  }
}

export interface ISendChatErrorData {
  error: IError;
}

export class SendChatError extends BaseResponse implements IResponse {
  constructor(public data: ISendChatErrorData) {
    super(false, CommandType.SendChatError);
  }
}

export interface IChatSentBroadcastData {
  userId: string;
  roomKey: string;
  chat: string;
}

export class ChatSentBroadcast extends BaseCommand implements ICommand {
  constructor(public data: IChatSentBroadcastData) {
    super(CommandType.ChatSentBroadcast);
  }
}

/**
 * The interface for a command.
 * 
 * @mermaid Make TypeDoc easy to use with mermaid.js
 * sequenceDiagram
 *  Alice->>+John: Hello John, how are you?
 *  John-->>-Alice: Great!
 *
 * @export
 * @interface ICommand
 */
export interface ICommand {
  /**
   * The command type.
   *
   * @type {CommandType}
   * @memberof ICommand
   */
  type: CommandType;
  /**
   * Epoch time the command was created.
   *
   * @type {number}
   * @memberof ICommand
   */
  createdAt: number;
  /**
   * Data contained in the command.
   *
   * @type {{ [key: string]: any }}
   * @memberof ICommand
   */
  data: { [key: string]: any };

  toString(): string;
}

/**
 * Broadcasts must go to a room.
 *
 * @export
 * @interface IBroadcastData
 */
export interface IBroadcastData {
  room: string;
}

/**
 * An IResponse interface.
 *
 * @export
 * @interface IResponse
 * @extends {ICommand}
 */
export interface IResponse extends ICommand {
  /**
   * Denotes if the Response was successful or not.
   *
   * @type {boolean}
   * @memberof IResponse
   */
  success: boolean;
}

/**
 * An IError interface.
 *
 * @export
 * @interface IError
 */
export interface IError {
  /**
   * Contains information about the error.
   *
   * @type {{ [key: string]: any }}
   * @memberof IError
   */
  info: { [key: string]: any };
}

/**
 * Types of commands that can be sent to and from the websocket server.
 *
 * @export
 * @enum {number}
 */
export enum CommandType {
  /**
   * Sent to all other members when a chat is sent by a member.
   * 
   * See [ChatSentBroadcast](classes/chatsentbroadcast.html).
   */
  ChatSentBroadcast = "ChatSentBroadcast",
  /**
   * Request to have a room created.
   * 
   * See [CreateRoomRequest](classes/createroomrequest.html).
   */
  CreateRoomRequest = "CreateRoomRequest",
  /**
   * Response when a room is successfully created.
   * 
   * See [CreateRoomSuccess](classes/createroomsuccess.html).
   */
  CreateRoomSuccess = "CreateRoomSuccess",
  /**
   * Response when there is an error creating a room.
   * 
   * See [CreateRoomError](classes/createroomerror.html).
   */
  CreateRoomError = "CreateRoomError",
  /** 
   * Request to flip a game tile over. 
   * 
   * See [FlipTileRequest](classes/fliptilerequest.html).
   */
  FlipTileRequest = "FlipTileRequest",
  /**
   * Sent to all users in a game when a tile is flipped over.
   * 
   * See [TileFlippedNotification](classes/tileflippednotification.html).
   */
  TileFlippedNotification = "TileFlippedNotification",
  HostChangedNotification = "HostChangedNotification",
  JoinRoomRequest = "JoinRoomRequest",
  JoinRoomSuccess = "JoinRoomSuccess",
  JoinRoomError = "JoinRoomError",
  JoinedRoomNotification = "JoinedRoomNotification",
  SendChatRequest = "SendChatRequest",
  SendChatSuccess = "SendChatSuccess",
  SendChatError = "SendChatError",
  UserReadinessBroadcast = "UserReadinessBroadcast",
  UserReadyNotification = "UserReadyNotification",
  UserLeavingRoomBroadcast = "UserLeavingRoomBroadcast",
  UserLeftRoomNotification = "UserLeftRoomNotification",
  UserPressingKeyBroadcast = "UserPressingKeyBroadcast",
  UserPressedKeyNotification = "UserPressedKeyNotification",
  UserInfoQuery = "UserInfoQuery",
  UserInfoResponse = "UserInfoResponse",
  StartGameRequest = "StartGameRequest",
  StartGameSuccess = "StartGameSuccess",
  StartGameError = "StartGameError",
  GameStartedNotification = "GameStartedNotification",
}

/**
 * The Base Command that's sent via websockets.
 *
 * @export
 * @abstract
 * @class BaseCommand
 * @implements {ICommand}
 */
export abstract class BaseCommand implements ICommand {
  /**
   * The epoch time that the command was created.
   *
   * @type {number}
   * @memberof BaseCommand
   */
  public createdAt: number;
  /**
   * Data contained in the command.
   *
   * @abstract
   * @type {{ [key: string]: any }}
   * @memberof BaseCommand
   */
  abstract data: { [key: string]: any };

  protected constructor(public type: CommandType) {
    this.createdAt = Date.now();
  }

  public toString() {
    return JSON.stringify(this);
  }
}

/**
 * A Response from a Command over websockets.
 *
 * @export
 * @abstract
 * @class BaseResponse
 * @extends {BaseCommand}
 * @implements {IResponse}
 */
export abstract class BaseResponse extends BaseCommand implements IResponse {
  protected constructor(public success: boolean, type: CommandType) {
    super(type);
  }
}

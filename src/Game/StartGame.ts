import type { IGameOptions } from "./Options";
import {
  BaseCommand,
  BaseResponse,
  CommandType,
  ICommand,
  IError,
  IResponse,
} from "../Command";
import type { IPlayer } from "../Users/Player";

/**
 * Data contained in [StartGameRequest](classes/startgamerequest.html).
 *
 * @export
 * @interface IStartGameRequestData
 */
export interface IStartGameRequestData {
  /**
   * Players in the game.
   *
   * @type {IPlayer[]}
   * @memberof IStartGameRequestData
   */
  players: IPlayer[];
  /**
   * The room key the players are currently in.
   *
   * @type {string}
   * @memberof IStartGameRequestData
   */
  roomKey: string;
  /**
   * Options for the game's creation and gameplay.
   *
   * @type {IGameOptions}
   * @memberof IStartGameRequestData
   */
  options: IGameOptions;
}

/**
 * A request to start a game.
 *
 * @export
 * @class StartGameRequest
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class StartGameRequest extends BaseCommand implements ICommand {
  constructor(public data: IStartGameRequestData) {
    super(CommandType.StartGameRequest);
  }
}

/**
 * This interface is purposely left blank.
 *
 * @export
 * @interface IStartGameSuccessData
 */
export interface IStartGameSuccessData {}

/**
 * Response when a game is started successfully.
 *
 * @export
 * @class StartGameSuccess
 * @extends {BaseResponse}
 * @implements {IResponse}
 */
export class StartGameSuccess extends BaseResponse implements IResponse {
  constructor(public data: IStartGameSuccessData) {
    super(true, CommandType.StartGameSuccess);
  }
}

/**
 * Data contained in a [StartGameError](classes/startgameerror.html) Response.
 *
 * @export
 * @interface IStartGameErrorData
 */
export interface IStartGameErrorData {
  /**
   * The error generated when trying to start a game.
   *
   * @type {IError}
   * @memberof IStartGameErrorData
   */
  error: IError;
}

/**
 * Response when there is an error starting a game.
 *
 * @export
 * @class StartGameError
 * @extends {BaseResponse}
 * @implements {IResponse}
 */
export class StartGameError extends BaseResponse implements IResponse {
  constructor(public data: IStartGameErrorData) {
    super(false, CommandType.StartGameError);
  }
}

/**
 * Data contained in a [GameStartedNotification](classes/gamestartednotification.html).
 *
 * @export
 * @interface IGameStartedNotificationData
 */
export interface IGameStartedNotificationData {
  /**
   * The key or id of the game that has started.
   *
   * @type {string}
   * @memberof IGameStartedNotificationData
   */
  key: string;
  /**
   * The game state.
   *
   * @type {*}
   * @memberof IGameStartedNotificationData
   */
  game: any;
}

/**
 * Sent to all users in a room when the game has started.
 *
 * @export
 * @class GameStartedNotification
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class GameStartedNotification extends BaseCommand implements ICommand {
  constructor(public data: IGameStartedNotificationData) {
    super(CommandType.GameStartedNotification);
  }
}

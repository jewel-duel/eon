import {
  BaseCommand,
  BaseResponse,
  CommandType,
  ICommand,
  IError,
  IResponse,
} from "../Command";
import type { IUser } from "../Users/User";
import type { IPlayer } from "../Users/Player";

//#region Request

/**
 * Data for a [JoinRoomRequest](../classes/joinroomrequest.html).
 *
 * @export
 * @interface IJoinRoomRequestData
 */
export interface IJoinRoomRequestData {
  /**
   * The key of the room to join.
   *
   * @type {string}
   * @memberof IJoinRoomRequestData
   */
  key: string;
  /**
   * The user requesting to join the room.
   *
   * @type {IUser}
   * @memberof IJoinRoomRequestData
   */
  user: IUser;
}

/**
 * A request to join a room.
 *
 * @export
 * @class JoinRoomRequest
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class JoinRoomRequest extends BaseCommand implements ICommand {
  public constructor(public data: IJoinRoomRequestData) {
    super(CommandType.JoinRoomRequest);
  }
}

//#endregion

//#region Response

/**
 * Data in a [JoinRoomSuccess](../classes/joinroomsuccess.html) Result.
 *
 * @export
 * @interface IJoinRoomSuccessData
 */
export interface IJoinRoomSuccessData {
  /**
   * The key of the room being joined.
   *
   * @type {string}
   * @memberof IJoinRoomSuccessData
   */
  key: string;
  /**
   * The host of the room.
   *
   * @type {IPlayer}
   * @memberof IJoinRoomSuccessData
   */
  host: IPlayer;
  /**
   * Other users that are in the room.
   *
   * @type {IPlayer[]}
   * @memberof IJoinRoomSuccessData
   */
  guests: IPlayer[];
  /**
   * Any game state that exists in the room.
   *
   * @type {*}
   * @memberof IJoinRoomSuccessData
   */
  game?: any;
}

/**
 * A successful result of a [JoinRoomRequest](../classes/joinroomrequest.html).
 *
 * @export
 * @class JoinRoomSuccess
 * @extends {BaseResponse}
 * @implements {IResponse}
 */
export class JoinRoomSuccess extends BaseResponse implements IResponse {
  constructor(public data: IJoinRoomSuccessData) {
    super(true, CommandType.JoinRoomSuccess);
  }
}

/**
 * An unsuccessful result of a [JoinRoomRequest](../classes/joinroomrequest.html).
 *
 * @export
 * @class JoinRoomError
 * @extends {BaseResponse}
 * @implements {IResponse}
 */
export class JoinRoomError extends BaseResponse implements IResponse {
  constructor(public data: IError) {
    super(false, CommandType.JoinRoomError);
  }
}

//#endregion Response

//#region Notification

/**
 * Data in a [JoinedRoomNotification](../classes/joinedroomnotification.html).
 *
 * @export
 * @interface IJoinedRoomNotification
 */
export interface IJoinedRoomNotification {
  /**
   * The player that joined the room.
   *
   * @type {IPlayer}
   * @memberof IJoinedRoomNotification
   */
  user: IPlayer;
}

/**
 * Sent to everyone in a room when a user joins.
 *
 * @export
 * @class JoinedRoomNotification
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class JoinedRoomNotification extends BaseCommand implements ICommand {
  constructor(public data: IJoinedRoomNotification) {
    super(CommandType.JoinedRoomNotification);
  }
}

//#endregion

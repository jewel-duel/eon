import { BaseCommand, CommandType, IBroadcastData, ICommand } from "../Command";

/**
 * Data contained in a [UserLeaveRoomBroadcast](../classes/userleaveroombroadcast.html).
 *
 * @export
 * @interface IUserLeaveRoomBroadcastData
 * @extends {IBroadcastData}
 */
export interface IUserLeaveRoomBroadcastData extends IBroadcastData {
  /**
   * The room the user is leaving.
   *
   * @type {string}
   * @memberof IUserLeaveRoomBroadcastData
   */
  roomKey: string;
  /**
   * The user leaving the room.
   *
   * @type {string}
   * @memberof IUserLeaveRoomBroadcastData
   */
  userId: string;
}

/**
 * Broadcast to all remaining users in a room when a user leaves the room.
 *
 * @export
 * @class UserLeavingRoomBroadcast
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class UserLeavingRoomBroadcast extends BaseCommand implements ICommand {
  constructor(public data: IUserLeaveRoomBroadcastData) {
    super(CommandType.UserLeavingRoomBroadcast);
  }
}

/**
 * Data contained in a [UserLeftRoomNotification](../classes/userleftroomnotification.html).
 *
 * @export
 * @interface IUserLeftRoomNotificationData
 */
export interface IUserLeftRoomNotificationData {
  /**
   * The user id of the user leaving the room.
   *
   * @type {string}
   * @memberof IUserLeftRoomNotificationData
   */
  userId: string;
}

/**
 * A notification sent to everyone in the room when a User leaves a Room.
 *
 * @export
 * @class UserLeftRoomNotification
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class UserLeftRoomNotification extends BaseCommand implements ICommand {
  constructor(public data: IUserLeftRoomNotificationData) {
    super(CommandType.UserLeftRoomNotification);
  }
}

/**
 * Data contained in a [HostChangedNotification](../classes/hostchangednotification.html).
 *
 * @export
 * @interface IHostChangedNotificationData
 */
export interface IHostChangedNotificationData {
  /**
   * The User ID of the new Host of the Room.
   *
   * @type {string}
   * @memberof IHostChangedNotificationData
   */
  userId: string;
}

/**
 * Sent to all members of a Room when a Host is changed to a different User.
 *
 * @export
 * @class HostChangedNotification
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class HostChangedNotification extends BaseCommand implements ICommand {
  constructor(public data: IHostChangedNotificationData) {
    super(CommandType.HostChangedNotification);
  }
}

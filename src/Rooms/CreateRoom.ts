import {
  BaseCommand,
  BaseResponse,
  CommandType,
  ICommand,
  IError,
  IResponse,
} from "../Command";
import type { IUser } from "../Users/User";
import type { IPlayer } from "../Users/Player";

/**
 * Data for the [CreateRoomRequest](../classes/createroomrequest.html) command.
 *
 * @export
 * @interface ICreateRoomRequestData
 */
export interface ICreateRoomRequestData {
  /**
   * The User creating the room.
   * 
   * They will automatically be made the host of the room and can edit the game settings.
   *
   * @type {IUser}
   * @memberof ICreateRoomRequestData
   */
  user: IUser;
}

/**
 * A Request to create a room.
 *
 * @export
 * @class CreateRoomRequest
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class CreateRoomRequest extends BaseCommand implements ICommand {
  constructor(public data: ICreateRoomRequestData) {
    super(CommandType.CreateRoomRequest);
  }
}

/**
 * Data for the [CreateRoomSuccess](../classes/createroomsuccess.html) Response.
 *
 * @export
 * @interface ICreateRoomSuccessData
 */
export interface ICreateRoomSuccessData {
  /**
   * The key of the room that was created.
   *
   * @type {string}
   * @memberof ICreateRoomSuccessData
   */
  key: string;
  /**
   * The Host of the room.
   *
   * @type {IPlayer}
   * @memberof ICreateRoomSuccessData
   */
  host: IPlayer;
}

/**
 * A Response when a [CreateRoomRequest](./createroomrequest.html) is successful.
 *
 * @export
 * @class CreateRoomSuccess
 * @extends {BaseResponse}
 * @implements {IResponse}
 */
export class CreateRoomSuccess extends BaseResponse implements IResponse {
  constructor(public data: ICreateRoomSuccessData) {
    super(true, CommandType.CreateRoomSuccess);
  }
}

/**
 * Data for the [CreateRoomError](../classes/createroomerror.html) Response.
 *
 * @export
 * @interface ICreateRoomErrorData
 */
export interface ICreateRoomErrorData {
  /**
   * The error during room creation.
   *
   * @type {IError}
   * @memberof ICreateRoomErrorData
   */
  error: IError;
}

/**
 * The Response when a [CreateRoomRequest](./createroomrequest.html) is unsuccessful.
 *
 * @export
 * @class CreateRoomError
 * @extends {BaseResponse}
 * @implements {IResponse}
 */
export class CreateRoomError extends BaseResponse implements IResponse {
  constructor(public data: IError) {
    super(false, CommandType.CreateRoomError);
  }
}

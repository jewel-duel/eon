export interface IPlayer {
  id: string;
  name: string;
  color: string;
}

/**
 * Data about the player.
 *
 * @category Domain
 * @export
 * @class Player
 * @implements {IPlayer}
 */
export class Player implements IPlayer {
  constructor(public id: string, public name: string, public color: string) {}
}

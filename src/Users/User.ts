import { BaseCommand, CommandType, ICommand } from "../Command";

/**
 * An IUser interface
 *
 * @export
 * @interface IUser
 */
export interface IUser {
  /**
   * The ID of the user.
   *
   * @type {string}
   * @memberof IUser
   */
  id: string;
  /**
   * The Name of the user.
   *
   * @type {string}
   * @memberof IUser
   */
  name: string;
}

/**
 * A User Object
 *
 * @export
 * @class User
 * @implements {IUser}
 */
export class User implements IUser {
  constructor(public id: string, public name: string) {}
}

/**
 * The IUserInfoQueryData interface is empty.
 *
 * @export
 * @interface IUserInfoQueryData
 */
export interface IUserInfoQueryData {}

/**
 * Sent to the user to ask for basic user information.
 *
 * @export
 * @class UserInfoQuery
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class UserInfoQuery extends BaseCommand implements ICommand {
  constructor(public data: IUserInfoQueryData) {
    super(CommandType.UserInfoQuery);
  }
}
/**
 * Data that's contained in the UserInfoResponse Command
 *
 * @export
 * @interface IUserInfoResponseData
 */
export interface IUserInfoResponseData {
  /**
   * The user data as part of the response.
   *
   * @type {IUser}
   * @memberof IUserInfoResponseData
   */
  user: IUser;
}

/**
 * Response for the UserInfoQuery Command.
 *
 * @export
 * @class UserInfoResponse
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class UserInfoResponse extends BaseCommand implements ICommand {
  constructor(public data: IUserInfoResponseData) {
    super(CommandType.UserInfoResponse);
  }
}

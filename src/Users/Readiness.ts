import type { IUser } from "./User";
import { BaseCommand, CommandType, IBroadcastData, ICommand } from "../Command";

/**
 * Data for when the User is ready
 *
 * @export
 * @interface IUserReadyBroadcastData
 * @extends {IBroadcastData}
 */
export interface IUserReadyBroadcastData extends IBroadcastData {
  /**
   * The current room the user is in.
   *
   * @type {string}
   * @memberof IUserReadyBroadcastData
   */
  roomKey: string;
  /**
   * The data of the user who is ready.
   *
   * @type {IUser}
   * @memberof IUserReadyBroadcastData
   */
  user: IUser;
}

/**
 * Sent to all the other users of the room that a user is now ready.
 *
 * @export
 * @class UserReadyBroadcast
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class UserReadyBroadcast extends BaseCommand implements ICommand {
  constructor(public data: IUserReadyBroadcastData) {
    super(CommandType.UserReadinessBroadcast);
  }
}

/**
 * Data contained in the [UserReadyNotification](../classes/userreadynotification.html)
 *
 * @export
 * @interface IUserReadyNotificationData
 */
export interface IUserReadyNotificationData {
  /**
   * Data of the User whose ready state is changing.
   *
   * @type {IUser}
   * @memberof IUserReadyNotificationData
   */
  user: IUser;
  /**
   * Boolean to indicate if the user is changing to ready or not ready.
   *
   * @type {boolean}
   * @memberof IUserReadyNotificationData
   */
  readiness: boolean;
}

/**
 * I don't know what this is for. :/
 *
 * @export
 * @class UserReadyNotification
 * @extends {BaseCommand}
 * @implements {ICommand}
 */
export class UserReadyNotification extends BaseCommand implements ICommand {
  constructor(public data: IUserReadyNotificationData) {
    super(CommandType.UserReadyNotification);
  }
}

# Eon

A library that contains websocket events for the Jewel Duel Game.

## Communication Workflows

- [General](modules/workflows_general.html)
- [Game](modules/workflows_game.html)

### Room Events

#### Creating a Room

Any user can create a room.

- User sends a [CreateRoomRequest](classes/createroomrequest.html).
- If successful, server responds with a [CreateRoomSuccess](classes/createroomsuccess.html) Response.
- If unsuccessful, server responds with a [CreateRoomError](classes/createroomerror.html) Response.

#### Joining a Room

Any user can join a room.

- User sends a [JoinRoomRequest](classes/joinroomrequest.html).
- If successful, server responds with a [JoinRoomSuccess](classes/joinroomsuccess.html) Response.
- If unsuccessful, server responds with a [JoinRoomError](classes/joinroomerror.html) Response.

#### Leaving a Room

A User in a room can leave at any time. The other users are notified with a [UserLeftRoomNotification](classes/userleftroomnotification.html) when this happens.

If the user who left was the host of the room, a new host is assigned by the server and a [HostChangedNotification](classes/hostchangednotification.html) is sent to all of the users in the room.

#### Sending a Chat

#### Keypresses

#### Changing Game Options

#### Changing Player Color

#### Setting Ready State for a Player

#### Starting the Game

### Game Events

#### Game Started Notification
